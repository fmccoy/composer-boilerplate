<?php

namespace Fmccoy;

/**
 * Test Class
 *
 * A simple test class
 *
 * @author Frank McCoy <fmccoy@thisisamg.com>
 * @version 1.0
 */

class Test{
	/**
	 * Construct
	 */
	public function __construct(){
		$this->run();
	}

	/**
	 * Run
	 * @return string 
	 */
	public function run(){
		echo '<h1>Hello World!</h1>';
	}
}